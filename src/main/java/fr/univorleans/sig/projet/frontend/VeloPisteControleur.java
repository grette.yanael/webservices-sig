package fr.univorleans.sig.projet.frontend;


import fr.univorleans.sig.projet.backend.dao.PisteRepository;
import fr.univorleans.sig.projet.backend.dao.StationplusRepository;
import fr.univorleans.sig.projet.backend.dao.StationveloRepository;
import fr.univorleans.sig.projet.backend.dao.VeloRepository;
import fr.univorleans.sig.projet.backend.facade.SigService;
import fr.univorleans.sig.projet.backend.modele.appli.Parcvelo;
import fr.univorleans.sig.projet.backend.modele.appli.Pistecyclable;
import fr.univorleans.sig.projet.backend.modele.appli.Stationvelo;
import fr.univorleans.sig.projet.backend.modele.appli.Stationveloplus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collection;

@Controller
@RequestMapping("/cyclable")
public class VeloPisteControleur {

    @Autowired
    private VeloRepository veloRepository;

    @Autowired
    private PisteRepository pisteRepository;

    @Autowired
    private StationveloRepository stationveloRepository;

    @Autowired
    private StationplusRepository stationplusRepository;

    @Autowired
    private SigService sigService;


    //pistes cyclables
    @GetMapping(value = "/pistes", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Pistecyclable>> getAllPiste() {
        Collection<Pistecyclable> pistes=pisteRepository.findAll();
        return ResponseEntity.ok().body(pistes);
    }

    @GetMapping(value = "/piste", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Pistecyclable> getPiste(@RequestParam("idPiste")Long idPiste) {
        Pistecyclable piste=pisteRepository.findByIdpiste(idPiste);
        return ResponseEntity.ok().body(piste);
    }



    @PostMapping(value = "/piste", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> noterPiste(@RequestParam("idPiste")Long idPiste,@RequestParam("note")double note){
        try{
            double moy=sigService.saveNotePiste(idPiste,note);
            return ResponseEntity.ok(""+moy);
        }
        catch(RuntimeException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (IOException e2) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e2.getMessage());
        }
    }

    //parcvelos
    @GetMapping(value = "/parc", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Parcvelo> getParc(@RequestParam("idParc")Long idParc) {
        Parcvelo parcvelo = veloRepository.findByIdparcv(idParc);
        return ResponseEntity.ok().body(parcvelo);
    }

    @GetMapping(value = "/parcs", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Parcvelo>> getAllParc() {
        Collection<Parcvelo> parcvelos=veloRepository.findAll();
        return ResponseEntity.ok().body(parcvelos);
    }

    @GetMapping(value = "/nomparc", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Parcvelo> getParcByNom(@RequestParam("nomParc")String nomParc) {
        Parcvelo parcvelo = veloRepository.findByNomarretparc(nomParc);
        return ResponseEntity.ok().body(parcvelo);
    }

    //stations vélos
    @GetMapping(value = "/stations", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Stationvelo>> getAllStationvelos() {
        Collection<Stationvelo> stationvelos = stationveloRepository.findAll();
        return ResponseEntity.ok().body(stationvelos);
    }

    @GetMapping(value = "/station", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Stationvelo> getStation(@RequestParam("idStation")Long idStation) {
        Stationvelo stationvelo = stationveloRepository.findByIdstation(idStation);
        return ResponseEntity.ok().body(stationvelo);
    }

    //stations vélos plus
    @GetMapping(value = "/stationsplus", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Stationveloplus>> getAllStationvelosplus() {
        Collection<Stationveloplus> stationvelosplus = stationplusRepository.findAll();
        return ResponseEntity.ok().body(stationvelosplus);
    }

    @GetMapping(value = "/stationplus", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Stationveloplus> getStationplus(@RequestParam("idStationplus")Long idStationplus) {
        Stationveloplus stationveloplus = stationplusRepository.findByIdstationplus(idStationplus);
        return ResponseEntity.ok().body(stationveloplus);
    }

}
