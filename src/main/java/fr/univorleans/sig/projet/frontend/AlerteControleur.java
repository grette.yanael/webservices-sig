package fr.univorleans.sig.projet.frontend;


import fr.univorleans.sig.projet.backend.dao.*;
import fr.univorleans.sig.projet.backend.facade.SigService;
import fr.univorleans.sig.projet.backend.modele.appli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/alerte")
public class AlerteControleur {

    @Autowired
    private AlertearretRepository alertearretRepository;

    @Autowired
    private AlerteligneRepository alerteligneRepository;

    @Autowired
    private AlerteparcRepository alerteparcRepository;

    @Autowired
    public AlertepisteRepository alertepisteRepository;

    @Autowired
    private AlertestationveloRepository alertestationveloRepository;

    @Autowired
    public AlerteveloplusRepository alerteveloplusRepository;

    @Autowired
    private SigService sigService;

    //arret
    @GetMapping(value = "/arret", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Alertearret>> getAllAlerteArret() {
        Collection<Alertearret> a= alertearretRepository.findAll();
        return ResponseEntity.ok().body(a);
    }

    @GetMapping(value = "/nbarret", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Integer> getNbAlerteArret() {
       Collection<Alertearret> a= alertearretRepository.findAll();
        return ResponseEntity.ok().body(a.size());
    }

    @PostMapping(value = "/arret", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> postAlerteArret(@Valid @RequestBody Alertearret alerte) {
        try{
            sigService.saveAlertearret(alerte.getArret().getIdarret(),alerte.getTitrealerte(),alerte.getType(),alerte.getTextalert(),alerte.getCptresolu());
            return ResponseEntity.ok("alerte enregistrée");
        }
        catch(RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    //ligne
    @GetMapping(value = "/ligne", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Alerteligne>> getAllAlerteLigne() {
        Collection<Alerteligne> a= alerteligneRepository.findAll();
        return ResponseEntity.ok().body(a);
    }

    @GetMapping(value = "/nbligne", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Integer> getNbAlerteLigne() {
        Collection<Alerteligne> a= alerteligneRepository.findAll();
        return ResponseEntity.ok().body(a.size());
    }

    @PostMapping(value = "/ligne", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> postAlerteLigne(@Valid @RequestBody Alerteligne alerte) {
        try{
            sigService.saveAlerteligne(alerte.getLigne().getNumligne(),alerte.getTitrealerte(),alerte.getType(),alerte.getTextalert(),alerte.getCptresolu());
            return ResponseEntity.ok("alerte enregistrée");
        }
        catch(RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    //parc
    @GetMapping(value = "/parc", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Alerteparc>> getAllAlerteParc() {
        Collection<Alerteparc> a= alerteparcRepository.findAll();
        return ResponseEntity.ok().body(a);
    }

    @GetMapping(value = "/nbparc", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Integer> getNbAlerteParc() {
        Collection<Alerteparc> a= alerteparcRepository.findAll();
        return ResponseEntity.ok().body(a.size());
    }

    @PostMapping(value = "/parc", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> postAlerteparc(@Valid @RequestBody Alerteparc alerte) {
        try{
            sigService.saveAlerteparc(alerte.getParcvelo().getIdparcv(),alerte.getTitrealerte(),alerte.getType(),alerte.getTextalert(),alerte.getCptresolu());
            return ResponseEntity.ok("alerte enregistrée");
        }
        catch(RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    //piste
    @GetMapping(value = "/piste", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Alertepiste>> getAllAlertePiste() {
        Collection<Alertepiste> a= alertepisteRepository.findAll();
        return ResponseEntity.ok().body(a);
    }

    @GetMapping(value = "/nbpiste", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Integer> getNbAlertePiste() {
        Collection<Alertepiste> a= alertepisteRepository.findAll();
        return ResponseEntity.ok().body(a.size());
    }

    @PostMapping(value = "/piste", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> postAlertepiste(@Valid @RequestBody Alertepiste alerte) {
        try{
            sigService.saveAlertepiste(alerte.getPistecyclable().getIdpiste(),alerte.getTitrealerte(),alerte.getType(),alerte.getTextalert(),alerte.getCptresolu());
            return ResponseEntity.ok("alerte enregistrée");
        }
        catch(RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    //station vélo
    @GetMapping(value = "/velo", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Alertestationvelo>> getAllAlerteVelo() {
        Collection<Alertestationvelo> a= alertestationveloRepository.findAll();
        return ResponseEntity.ok().body(a);
    }

    @GetMapping(value = "/nbvelo", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Integer> getNbAlerteVelo() {
        Collection<Alertestationvelo> a= alertestationveloRepository.findAll();
        return ResponseEntity.ok().body(a.size());
    }


    @PostMapping(value = "/stationvelo", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> postAlertestation(@Valid @RequestBody Alertestationvelo alerte) {
        try{
            sigService.saveAlertestationvelo(alerte.getStationvelo().getIdstation(),alerte.getTitrealerte(),alerte.getType(),alerte.getTextalert(),alerte.getCptresolu());
            return ResponseEntity.ok("alerte enregistrée");
        }
        catch(RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    //station vélo plus
    @GetMapping(value = "/plus", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Collection<Alerteveloplus>> getAllAlertePlus() {
        Collection<Alerteveloplus> a= alerteveloplusRepository.findAll();
        return ResponseEntity.ok().body(a);
    }

    @GetMapping(value = "/nbplus", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Integer> getNbAlertePlus() {
        Collection<Alerteveloplus> a= alerteveloplusRepository.findAll();
        return ResponseEntity.ok().body(a.size());
    }


    @PostMapping(value = "/stationplus", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> postAlertestationplus(@Valid @RequestBody Alerteveloplus alerte) {
        try{
            sigService.saveAlertestationplus(alerte.getStationveloplus().getIdstationplus(),alerte.getTitrealerte(),alerte.getType(),alerte.getTextalert(),alerte.getCptresolu());
            return ResponseEntity.ok("alerte enregistrée");
        }
        catch(RuntimeException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping(value = "/map")
    public String test() {
        return "transport/ligne1";
    }


}
