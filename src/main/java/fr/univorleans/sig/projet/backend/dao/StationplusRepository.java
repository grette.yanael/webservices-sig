package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Stationvelo;
import fr.univorleans.sig.projet.backend.modele.appli.Stationveloplus;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface StationplusRepository extends CrudRepository<Stationveloplus, Long> {
    public Collection<Stationveloplus> findAll();
    public Stationveloplus findByIdstationplus(long idstationplus);

}
