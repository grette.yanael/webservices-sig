package fr.univorleans.sig.projet.backend.modele.appli;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Stationvelo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idstation;
    @NotNull
    private String commune;
    @NotNull
    private String adressestation;
    @NotNull
    private String type;
    private int nbplace;

    @OneToMany( fetch = FetchType.EAGER, mappedBy = "stationvelo")
    private Collection<Alertestationvelo> alertestationvelos;

    public Stationvelo() {
        this.alertestationvelos = new ArrayList<Alertestationvelo>();
    }

    public long getIdstation() {
        return idstation;
    }

    public void setIdstation(long idstation) {
        this.idstation = idstation;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getAdressestation() {
        return adressestation;
    }

    public void setAdressestation(String adressestation) {
        this.adressestation = adressestation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNbplace() {
        return nbplace;
    }

    public void setNbplace(int nbplace) {
        this.nbplace = nbplace;
    }

    public Collection<Alertestationvelo> getAlertestationvelos() {
        return alertestationvelos;
    }

    public void setAlertestationvelos(Collection<Alertestationvelo> alertestationvelos) {
        this.alertestationvelos = alertestationvelos;
    }

    public void addAlertestationvelo(Alertestationvelo alertestationvelo) {
        alertestationvelo.setStationvelo(this);
        this.alertestationvelos.add(alertestationvelo);
    }
}
