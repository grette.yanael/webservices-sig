package fr.univorleans.sig.projet.backend.modele.appli;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Pistecyclable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idpiste;
    @NotNull
    private String commune;
    @NotNull
    private String rue;
    @NotNull
    private int longueur;
    @NotNull
    private String sens;
    private double note;
    private int nbvote;
    @OneToMany( fetch = FetchType.EAGER, mappedBy = "pistecyclable")
    private Collection<Alertepiste> alertepistes;

    public Pistecyclable() { this.alertepistes=new ArrayList<Alertepiste>();
    }

    public long getIdpiste() {
        return idpiste;
    }

    public void setIdpiste(long idpiste) {
        this.idpiste = idpiste;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public int getLongueur() {
        return longueur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public String getSens() {
        return sens;
    }

    public void setSens(String sens) {
        this.sens = sens;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    public int getNbvote() {
        return nbvote;
    }

    public void setNbvote(int nbvote) {
        this.nbvote = nbvote;
    }

    public Collection<Alertepiste> getAlertepistes() {
        return alertepistes;
    }

    public void setAlertepistes(Collection<Alertepiste> alertepistes) {
        this.alertepistes = alertepistes;
    }

    public void addAlertepiste(Alertepiste alertepiste) {
        alertepiste.setPistecyclable(this);
        this.alertepistes.add(alertepiste);
    }
}
