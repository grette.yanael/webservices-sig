package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.map.Maparret;
import fr.univorleans.sig.projet.backend.modele.map.Mapparcvelo;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface MapparcveloRepository extends CrudRepository<Mapparcvelo, Integer> {

    public Mapparcvelo findByIdparcv(int idparcvelo);

}
