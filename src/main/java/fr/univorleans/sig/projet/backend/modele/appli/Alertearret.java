package fr.univorleans.sig.projet.backend.modele.appli;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Entity
public class Alertearret {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idalerte;

    @JsonProperty(access = WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "idarret")
    private Arret arret;

    private String titrealerte;

    private String type; //détérioration, retard, coupure
    @NotNull
    private String textalert;

    private Date date;
    private int cptresolu;


    public Alertearret() {}

    public Alertearret(String titrealerte, String type, @NotNull String textalert, int cptresolu) {
        this.titrealerte = titrealerte;
        this.type = type;
        this.textalert = textalert;
        this.cptresolu = cptresolu;
    }

    public Long getIdalerte() {
        return idalerte;
    }

    public void setIdalerte(Long idalerte) {
        this.idalerte = idalerte;
    }

    public String getTitrealerte() {
        return titrealerte;
    }

    public void setTitrealerte(String titrealerte) {
        this.titrealerte = titrealerte;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTextalert() {
        return textalert;
    }

    public void setTextalert(String textalert) {
        this.textalert = textalert;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCptresolu() {
        return cptresolu;
    }

    public void setCptresolu(int cptresolu) {
        this.cptresolu = cptresolu;
    }

    public Arret getArret() {
        return arret;
    }

    public void setArret(Arret arret) {
        this.arret = arret;
    }


}
