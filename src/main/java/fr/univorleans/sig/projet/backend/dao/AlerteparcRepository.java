package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Alertearret;
import fr.univorleans.sig.projet.backend.modele.appli.Alerteparc;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface AlerteparcRepository extends CrudRepository<Alerteparc, Long> {

    public Collection<Alerteparc> findAll();

}
