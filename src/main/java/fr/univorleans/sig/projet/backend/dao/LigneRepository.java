package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Arret;
import fr.univorleans.sig.projet.backend.modele.appli.Ligne;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface LigneRepository extends CrudRepository<Ligne, String> {

    public Ligne findByNumligne(String numLigne);
    public Collection<Ligne> findAll();
    public Collection<Ligne> findAllByArrets(Arret idarret);

}
