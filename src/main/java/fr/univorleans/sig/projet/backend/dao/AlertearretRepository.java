package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Alertearret;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface AlertearretRepository extends CrudRepository<Alertearret, Long> {

    public Collection<Alertearret> findAll();

}
