package fr.univorleans.sig.projet.backend.modele.appli;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Entity
public class Alerteparc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idalerte;
    @JsonProperty(access = WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "idparcv")
    private Parcvelo parcvelo;

    private String titrealerte;

    private String type; //détérioration, retard, coupure
    @NotNull
    private String textalert;
    private Date date;
    private int cptresolu;


    public Alerteparc() {}

    public Alerteparc(String titrealerte, String type, @NotNull String textalert, int cptresolu) {
        this.titrealerte = titrealerte;
        this.type = type;
        this.textalert = textalert;
        this.cptresolu = cptresolu;
    }

    public long getIdalerte() {
        return idalerte;
    }

    public void setIdalerte(long idalerte) {
        this.idalerte = idalerte;
    }

    public Parcvelo getParcvelo() {
        return parcvelo;
    }

    public void setParcvelo(Parcvelo parcvelo) {
        this.parcvelo = parcvelo;
    }

    public String getTitrealerte() {
        return titrealerte;
    }

    public void setTitrealerte(String titrealerte) {
        this.titrealerte = titrealerte;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTextalert() {
        return textalert;
    }

    public void setTextalert(String textalert) {
        this.textalert = textalert;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCptresolu() {
        return cptresolu;
    }

    public void setCptresolu(int cptresolu) {
        this.cptresolu = cptresolu;
    }
}
