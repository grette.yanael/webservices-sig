package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Pistecyclable;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface PisteRepository extends CrudRepository<Pistecyclable,  Long> {
    public Collection<Pistecyclable> findAll();
    public Pistecyclable findByIdpiste(long idpiste);
    public Collection<Pistecyclable> findByCommune(String commune);
    public Pistecyclable findByRue(String rue);


}
