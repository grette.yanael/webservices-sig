package fr.univorleans.sig.projet.backend.modele.appli;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Parcvelo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idparcv;
    @NotNull
    private String code;
    @NotNull
    private String nomarretparc;
    @NotNull
    private String commune;
    @NotNull
    private String adresseparc;

    @OneToMany( fetch = FetchType.EAGER, mappedBy = "parcvelo")
    private Collection<Alerteparc> alerteparcs;

    public Parcvelo() {
        this.alerteparcs = new ArrayList<Alerteparc>();
    }

    public long getIdparcv() {
        return idparcv;
    }

    public void setIdparcv(long idparcv) {
        this.idparcv = idparcv;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNomarretparc() {
        return nomarretparc;
    }

    public void setNomarretparc(String nomarretparc) {
        this.nomarretparc = nomarretparc;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getAdresseparc() {
        return adresseparc;
    }

    public void setAdresseparc(String adresseparc) {
        this.adresseparc = adresseparc;
    }

    public Collection<Alerteparc> getAlerteparcs() {
        return alerteparcs;
    }

    public void setAlerteparcs(Collection<Alerteparc> alerteparcs) {
        this.alerteparcs = alerteparcs;
    }
    public void addAlerteparc(Alerteparc alerteparc) {
        alerteparc.setParcvelo(this);
        this.alerteparcs.add(alerteparc);
    }
}
