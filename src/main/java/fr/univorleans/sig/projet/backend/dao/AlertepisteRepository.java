package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Alertearret;
import fr.univorleans.sig.projet.backend.modele.appli.Alertepiste;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface AlertepisteRepository extends CrudRepository<Alertepiste, Long> {

    public Collection<Alertepiste> findAll();

}
