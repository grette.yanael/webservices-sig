package fr.univorleans.sig.projet.backend.modele.appli;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Ligne {

    @Id
    private String numligne;
    @NotNull
    private String direction1;
    @NotNull
    private String direction2;
    @NotNull
    private String route_color;
    @NotNull
    private String text_color;
    @OneToMany( fetch = FetchType.EAGER, mappedBy = "ligne")
    private Collection<Alerteligne> alertelignes;

    @ManyToMany( mappedBy = "lignes")
    @JsonIgnore
    private Collection<Arret> arrets;

    public Ligne() {
        this.alertelignes=new ArrayList<Alerteligne>();
        this.arrets=new ArrayList<Arret>();
    }

    public String getNumligne() {
        return numligne;
    }

    public void setNumligne(String numligne) {
        this.numligne = numligne;
    }

    public String getDirection1() {
        return direction1;
    }

    public void setDirection1(String direction1) {
        this.direction1 = direction1;
    }

    public String getDirection2() {
        return direction2;
    }

    public void setDirection2(String direction2) {
        this.direction2 = direction2;
    }

    public String getRoute_color() {
        return route_color;
    }

    public void setRoute_color(String route_color) {
        this.route_color = route_color;
    }

    public String getText_color() {
        return text_color;
    }

    public void setText_color(String text_color) {
        this.text_color = text_color;
    }

    public Collection<Alerteligne> getAlertelignes() {
        return alertelignes;
    }

    public void setAlertelignes(Collection<Alerteligne> alertelignes) {
        this.alertelignes = alertelignes;
    }

    public void addAlerteligne(Alerteligne alerteligne) {
        alerteligne.setLigne(this);
        this.alertelignes.add(alerteligne);
    }

    public Collection<Arret> getArrets() {
        return arrets;
    }

    public void setArrets(Collection<Arret> arrets) {
        this.arrets = arrets;
    }

    public void addArret(Arret arret) {
        this.arrets.add(arret);
    }
}
