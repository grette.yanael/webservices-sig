package fr.univorleans.sig.projet.backend.modele.map;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.geo.Point;
import org.springframework.data.geo.Polygon;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;

@Entity
public class Mapligne {
    @Id
    private String numligne;
    @NotNull
    private int zoom;
    @NotNull
    @Column(name="long")
    private float longitude;
    @NotNull
    @Column(name="lat")
    private float latitude;

    public String getNumligne() {
        return numligne;
    }

    public void setNumligne(String numligne) {
        this.numligne = numligne;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
}
