package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.appli.Alertearret;
import fr.univorleans.sig.projet.backend.modele.appli.Alerteligne;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface AlerteligneRepository extends CrudRepository<Alerteligne, Long> {

    public Collection<Alerteligne> findAll();

}
