package fr.univorleans.sig.projet.backend.facade;

import fr.univorleans.sig.projet.backend.dao.*;
import fr.univorleans.sig.projet.backend.modele.appli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class SigService {

    @Autowired
    private PisteRepository pisteRepository;

    @Autowired
    private ArretRepository arretRepository;

    @Autowired
    private AlertearretRepository alertearretRepository;

    @Autowired
    private AlerteligneRepository alerteligneRepository;

    @Autowired
    private LigneRepository ligneRepository;

    @Autowired
    private VeloRepository parcrepo;

    @Autowired
    private AlerteparcRepository alerteparcRepository;

    @Autowired
    private AlertepisteRepository alertepisteRepository;

    @Autowired
    private AlertestationveloRepository alertestationveloRepository;

    @Autowired
    private StationveloRepository stationveloRepository;

    @Autowired
    private AlerteveloplusRepository alerteveloplusRepository;

    @Autowired
    private StationplusRepository stationplusRepository;

    @Transactional(readOnly = false)
    public double saveNotePiste(long idPiste, double note) throws RuntimeException, IOException {
        if(note<0 || note>5){
            throw new IOException("la note doit être comprise entre 0 et 5");
        }
        Pistecyclable p=pisteRepository.findByIdpiste(idPiste);
        if(p==null)throw new RuntimeException("la piste "+idPiste+" n'existe pas");
        double moyenne=(p.getNote()*p.getNbvote()+note)/(p.getNbvote()+1);
        p.setNote(moyenne);
        p.setNbvote(p.getNbvote()+1);
        pisteRepository.save(p);
        return p.getNote();

    }

    public void saveAlertearret(long idarret, String titre, String type, String texte,int cptresolu)throws RuntimeException{
        Alertearret alertearret=new Alertearret(titre,type,texte,cptresolu);
        Arret arret= arretRepository.findByIdarret(idarret);
        if(arret==null)throw new RuntimeException("l'arret "+idarret+" n'existe pas");
        alertearret.setArret(arret);
        Date date = new Date();
        alertearret.setDate(date);
        alertearretRepository.save(alertearret);
    }

    public void saveAlerteligne(String numligne, String titre, String type, String texte,int cptresolu)throws RuntimeException{
        Alerteligne alerteligne=new Alerteligne(titre,type,texte,cptresolu);
        Ligne l= ligneRepository.findByNumligne(numligne);
        if(l==null)throw new RuntimeException("la ligne "+numligne+" n'existe pas");
        alerteligne.setLigne(l);
        Date date = new Date();

        alerteligne.setDate(date);
        alerteligneRepository.save(alerteligne);

    }

    public void saveAlerteparc(long idparcv, String titre, String type, String texte,int cptresolu)throws RuntimeException{
        Alerteparc a=new Alerteparc(titre,type,texte,cptresolu);
        Parcvelo p= parcrepo.findByIdparcv(idparcv);
        if(p==null)throw new RuntimeException("le parc "+idparcv+" n'existe pas");
        a.setParcvelo(p);
        Date date = new Date();
        a.setDate(date);
        alerteparcRepository.save(a);

    }

    public void saveAlertepiste(long idpiste, String titre, String type, String texte,int cptresolu)throws RuntimeException{
        Alertepiste a=new Alertepiste(titre,type,texte,cptresolu);
        Pistecyclable p= pisteRepository.findByIdpiste(idpiste);
        if(p==null)throw new RuntimeException("la piste cyclable "+idpiste+" n'existe pas");
        a.setPistecyclable(p);
        Date date = new Date();
        a.setDate(date);
        alertepisteRepository.save(a);

    }

    public void saveAlertestationvelo(long idstation, String titre, String type, String texte,int cptresolu)throws RuntimeException{
        Alertestationvelo a=new Alertestationvelo(titre,type,texte,cptresolu);
        Stationvelo s= stationveloRepository.findByIdstation(idstation);
        if(s==null)throw new RuntimeException("la station vélo "+idstation+" n'existe pas");
        a.setStationvelo(s);
        Date date = new Date();
        a.setDate(date);
        alertestationveloRepository.save(a);

    }

    public void saveAlertestationplus(long idstationplus, String titre, String type, String texte,int cptresolu)throws RuntimeException{
        Alerteveloplus a=new Alerteveloplus(titre,type,texte,cptresolu);
        Stationveloplus s= stationplusRepository.findByIdstationplus(idstationplus);
        if(s==null)throw new RuntimeException("la station vélo plus "+idstationplus+" n'existe pas");
        a.setStationveloplus(s);
        Date date = new Date();
        a.setDate(date);
        alerteveloplusRepository.save(a);

    }


}
