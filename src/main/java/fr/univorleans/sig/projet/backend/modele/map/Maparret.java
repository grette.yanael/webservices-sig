package fr.univorleans.sig.projet.backend.modele.map;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.geo.Point;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;

@Entity
public class Maparret {
    @Id
    private int idmap;
    @NotNull
    private String numligne;
    @NotNull
    private long idarret;
    @NotNull
    private String direction;
    @NotNull
    private int zoom;
    @NotNull
    @Column(name="long")
    private float longitude;
    @NotNull
    @Column(name="lat")
    private float latitude;

    public int getIdmap() {
        return idmap;
    }

    public void setIdmap(int idmap) {
        this.idmap = idmap;
    }

    public String getNumligne() {
        return numligne;
    }

    public void setNumligne(String numligne) {
        this.numligne = numligne;
    }

    public long getIdarret() {
        return idarret;
    }

    public void setIdarret(long idarret) {
        this.idarret = idarret;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
}
