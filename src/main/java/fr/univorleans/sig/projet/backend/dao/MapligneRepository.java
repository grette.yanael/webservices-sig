package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.map.Maparret;
import fr.univorleans.sig.projet.backend.modele.map.Mapligne;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface MapligneRepository extends CrudRepository<Mapligne, String> {

    public Mapligne findByNumligne(String numligne);

}
