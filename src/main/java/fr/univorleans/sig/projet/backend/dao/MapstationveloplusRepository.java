package fr.univorleans.sig.projet.backend.dao;

import fr.univorleans.sig.projet.backend.modele.map.Maparret;
import fr.univorleans.sig.projet.backend.modele.map.Mapstationveloplus;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface MapstationveloplusRepository extends CrudRepository<Mapstationveloplus, Integer> {

    public Mapstationveloplus findByIdstationplus(int idstationplus);

}
