package fr.univorleans.sig.projet.backend.modele.appli;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Arret {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idarret;
    @NotNull
    private String nomarret;

    @OneToMany(fetch = FetchType.EAGER, mappedBy ="arret")
    private Collection<Alertearret> alertearrets;

    @ManyToMany
    @JoinTable(name = "arretligne",joinColumns = @JoinColumn(name = "idarret"),inverseJoinColumns = @JoinColumn(name = "numligne"))
    private Collection<Ligne> lignes;

    public Arret() {
        this.alertearrets=new ArrayList<Alertearret>();
        this.lignes=new ArrayList<Ligne>();
    }

    public long getIdarret() {
        return idarret;
    }

    public void setIdarret(long idarret) {
        this.idarret = idarret;
    }

    public String getNomarret() {
        return nomarret;
    }

    public void setNomarret(String nomarret) {
        this.nomarret = nomarret;
    }

    public Collection<Alertearret> getAlertearrets() {
        return alertearrets;
    }

    public void addAlertearret(Alertearret alertearret) {
        alertearret.setArret(this);
        this.alertearrets.add(alertearret);
    }
    public void setAlertearrets(Collection<Alertearret> alertearrets) {
        this.alertearrets = alertearrets;
    }

    public Collection<Ligne> getLignes() {
        return lignes;
    }

    public void setLignes(Collection<Ligne> lignes) {
        this.lignes = lignes;
    }

    public void addLigne(Ligne ligne) {
        ligne.addArret(this);
        this.lignes.add(ligne);
    }
}
